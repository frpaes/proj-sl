﻿using SawLuzProj.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SawLuzProj.Context
{
    public class RestauranteContext : DbContext
    {
        public DbSet<Restaurantes> Restaurantes { get; set; }

        public DbSet<Pratos> Pratos { get; set; }
    }
}