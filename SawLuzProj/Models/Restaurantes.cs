﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SawLuzProj.Models
{
    public class Restaurantes
    {
        [Key]
        public int RestauranteId { get; set; }

        public string NomeRestaurante { get; set; }

        public ICollection<Pratos> Pratos { get; set; }
    }

}