﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SawLuzProj.Models
{
    public class Pratos
    {
        [Key]
        public int PratoId { get; set; }

        public string NomePrato { get; set; }

        public Decimal Preco { get; set; }

        public int RestauranteId { get; set; }
        public string NomeRestaurante { get; set; }
        public Restaurantes Restaurantes { get; set; }
    }
}