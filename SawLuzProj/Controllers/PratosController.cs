﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SawLuzProj.Models;
using SawLuzProj.Context;

namespace SawLuzProj.Controllers
{
    public class PratosController : Controller
    {
        private RestauranteContext db = new RestauranteContext();

        //
        // GET: /Pratos/

        public ActionResult Index()
        {
            return View(db.Pratos.ToList());
        }

        //
        // GET: /Pratos/Details/5

        public ActionResult Details(int id = 0)
        {
            Pratos pratos = db.Pratos.Find(id);
            if (pratos == null)
            {
                return HttpNotFound();
            }
            return View(pratos);
        }

        //
        // GET: /Pratos/Create

        public ActionResult Create()
        {
            var restaurantes = db.Restaurantes.ToList();
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "", Value = "" });
            foreach(var listRest in restaurantes )
            {
                items.Add(new SelectListItem { Text = listRest.NomeRestaurante, Value = listRest.RestauranteId.ToString() });
            }
            
            ViewData["ddlRestaurantes"] = items;
           

            return View();
        }

        //
        // POST: /Pratos/Create

        [HttpPost]
        public ActionResult Create(Pratos pratos, FormCollection form)
        {

            int selectedValues = int.Parse( form["ddlRestaurantes"]);

            Restaurantes restaurante = db.Restaurantes.Find(selectedValues);

            if (ModelState.IsValid)
            {
                db.Pratos.Add(pratos);
                pratos.NomeRestaurante = restaurante.NomeRestaurante;
                pratos.RestauranteId = restaurante.RestauranteId;
                pratos.Restaurantes = restaurante;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pratos);
        }

        //
        // GET: /Pratos/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Pratos pratos = db.Pratos.Find(id);
            if (pratos == null)
            {
                return HttpNotFound();
            }
            return View(pratos);
        }

        //
        // POST: /Pratos/Edit/5

        [HttpPost]
        public ActionResult Edit(Pratos pratos, FormCollection form)
        {
            if (ModelState.IsValid)
            {
                var rest = form["NomeRestaurante"];
                pratos.NomeRestaurante = rest;
                db.Entry(pratos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pratos);
        }

        //
        // GET: /Pratos/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Pratos pratos = db.Pratos.Find(id);
            if (pratos == null)
            {
                return HttpNotFound();
            }
            return View(pratos);
        }

        //
        // POST: /Pratos/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Pratos pratos = db.Pratos.Find(id);
            db.Pratos.Remove(pratos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}