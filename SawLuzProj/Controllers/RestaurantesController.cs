﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SawLuzProj.Models;
using SawLuzProj.Context;
using System.Threading.Tasks;

namespace SawLuzProj.Controllers
{
    public class RestaurantesController : Controller
    {
        private RestauranteContext db = new RestauranteContext();

        //
        // GET: /Restaurantes/

        public ActionResult Index(string filtro)
        {
            

            if (!String.IsNullOrEmpty(filtro))
            {
                return View(db.Restaurantes.Where(s => s.NomeRestaurante.Contains(filtro)));
            }
            else { return View(db.Restaurantes.ToList()); }
        }

        //
        // GET: /Restaurantes/Details/5

        public ActionResult Details(int id = 0)
        {
            Restaurantes restaurantes = db.Restaurantes.Find(id);
            if (restaurantes == null)
            {
                return HttpNotFound();
            }
            return View(restaurantes);
        }

        //
        // GET: /Restaurantes/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Restaurantes/Create

        [HttpPost]
        public ActionResult Create(Restaurantes restaurantes)
        {
            if (ModelState.IsValid)
            {
                db.Restaurantes.Add(restaurantes);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(restaurantes);
        }

        //
        // GET: /Restaurantes/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Restaurantes restaurantes = db.Restaurantes.Find(id);
            if (restaurantes == null)
            {
                return HttpNotFound();
            }
            return View(restaurantes);
        }

        //
        // POST: /Restaurantes/Edit/5

        [HttpPost]
        public ActionResult Edit(Restaurantes restaurantes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(restaurantes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(restaurantes);
        }

        //
        // GET: /Restaurantes/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Restaurantes restaurantes = db.Restaurantes.Find(id);
            if (restaurantes == null)
            {
                return HttpNotFound();
            }
            return View(restaurantes);
        }

        //
        // POST: /Restaurantes/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            foreach (Pratos prato in db.Pratos)
            {
                if (prato.RestauranteId == id)
                {
                    db.Pratos.Remove(prato);
                }
            }

            Restaurantes restaurantes = db.Restaurantes.Find(id);
            db.Restaurantes.Remove(restaurantes);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult Search(string searchString)
        {
            return View(db.Restaurantes.Where(x => x.NomeRestaurante.Contains(searchString)).OrderBy(x => x.NomeRestaurante));
        }

    }
}